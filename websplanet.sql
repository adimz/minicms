-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 07, 2014 at 08:25 PM
-- Server version: 5.5.35
-- PHP Version: 5.4.6-1ubuntu1.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `websplanet`
--

-- --------------------------------------------------------

--
-- Table structure for table `site`
--

CREATE TABLE IF NOT EXISTS `site` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `site_name` varchar(1024) NOT NULL,
  `domain` varchar(128) NOT NULL,
  `sub_domain` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=78 ;

--
-- Dumping data for table `site`
--

INSERT INTO `site` (`id`, `site_name`, `domain`, `sub_domain`) VALUES
(1, 'heaven', 'heaven.com', 'heaven.minicms.localhost'),
(73, 'adi', 'mizrahi.com', 'harp'),
(74, 'adi', 'mizrahi.com', 'harp'),
(75, 'adi', 'mizrahi.com', 'harxxp'),
(76, 'adi', 'mizrahi.com', 'harp'),
(77, 'adi', 'mizrahi.com', 'harp');

-- --------------------------------------------------------

--
-- Table structure for table `site_menu`
--

CREATE TABLE IF NOT EXISTS `site_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `site_id` bigint(20) NOT NULL,
  `language` varchar(32) NOT NULL,
  `title` varchar(1024) NOT NULL,
  `friendly_url` varchar(128) NOT NULL,
  `keywords` varchar(2048) NOT NULL,
  `description` varchar(1024) NOT NULL,
  `page_type` varchar(20) NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`),
  KEY `language` (`language`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `site_menu`
--

INSERT INTO `site_menu` (`id`, `site_id`, `language`, `title`, `friendly_url`, `keywords`, `description`, `page_type`, `content`) VALUES
(1, 1, 'en', 'Home', 'home', 'heaven, god, moses', 'another day in paradise', 'static', '<p>\r\nOh thinkin'' about all our younger years \r\nThere was only you and me \r\nWe were young and wild and free \r\nNow nothing can take you away from me \r\nWe''ve been down that road before \r\nBut that''s over now \r\nYou keep me comin'' back for more \r\n</p>\r\n<p>\r\nAnd baby you''re all that I want \r\nWhen you''re lyin'' here in my arms \r\nI''m findin'' it hard to believe \r\nWe''re in heaven \r\nAnd love is all that I need \r\nAnd I found it there in your heart \r\nIt isn''t too hard to see \r\nWe''re in heaven \r\n</p>'),
(2, 1, 'en', 'Contact US', 'contact_us', 'contact heaven, ', 'stairways to heaven', 'static', '<p>\r\nOh thinkin'' about all our younger years \r\nThere was only you and me \r\nWe were young and wild and free \r\nNow nothing can take you away from me \r\nWe''ve been down that road before \r\nBut that''s over now \r\nYou keep me comin'' back for more \r\n</p>\r\n<p>\r\nAnd baby you''re all that I want \r\nWhen you''re lyin'' here in my arms \r\nI''m findin'' it hard to believe \r\nWe''re in heaven \r\nAnd love is all that I need \r\nAnd I found it there in your heart \r\nIt isn''t too hard to see \r\nWe''re in heaven \r\n</p>'),
(3, 1, 'en', 'from json page', 'json', 'hi, bye', 'json server test', 'static', '<h3>$$$$</h3>'),
(4, 1, 'en', 'from json page', 'json2', 'hi, bye', 'json server test', 'static', '<h3>$$$$</h3>');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `site_menu`
--
ALTER TABLE `site_menu`
  ADD CONSTRAINT `site_menu_ibfk_1` FOREIGN KEY (`site_id`) REFERENCES `site` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
