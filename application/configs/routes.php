<?php

//handling subdomains
$hostnameRoute = new Zend_Controller_Router_Route_Hostname(
    ':site.minicms.localhost',
    array(
        'controller' => 'site',
        'action'     => 'view'
    )
);
$plainPathRoute = new Zend_Controller_Router_Route(':page/*');
$router->addRoute('user', $hostnameRoute->chain($plainPathRoute));


