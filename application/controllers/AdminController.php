<?php

class AdminController extends Zend_Controller_Action
{
    /* Usage:
    //todo POST one of this JSON structures + API_CODE=xxx at the URL to this controller
        { "method": "siteList", "params":[ <id> ,"params": null ,"id": null}
        { "method": "addSite", "params":[ {"site_name":"<name>","domain":"<domain>","sub_domain":"<sub.domain>"}], "id": null }
        { "method": "updateSite", "params":[ <id>,{"site_name":"<name>","domain":"<domain>","sub_domain":"<sub.domain>"}], "id": null }
        { "method": "deleteSite", "params":[ <id> ,"params":null, "id": null }
        --
        { "method": "getPagesList", "params":[ <site_id> ,"params": null ,"id": null}
        { "method": "addPage", "params":[ {"site_id":"<site_id>","language":"<(en\he)>","title":"<title>", "friendly_url":"<page_name>","keywords":"<SEO_keywords>","description":"<SEO_desc>","page_type":"<static/etc.>","content":"<HTML_content>"}], "id": null }
        { "method": "updatePage", "params":[ <id>,{"site_name":"<name>","domain":"<domain>","sub_domain":"<sub.domain>"}], "id": null }
        { "method": "deletePage", "params":[ <id> ,"params":null, "id": null }

*/

    public function init(){

        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        if(!isset($_GET['API_CODE']) || $_GET['API_CODE'] != "xxxx") //will be implemented with auth model later...
        {
            echo "auth error...!";
            return null;
        }

        $server = new Zend_Json_Server();
        $server->setClass('API');
        if ('GET' == $_SERVER['REQUEST_METHOD']) {
            // Indicate the URL endpoint, and the JSON-RPC version used:
            $server->setTarget('/json-rpc.php')
                ->setEnvelope(Zend_Json_Server_Smd::ENV_JSONRPC_2);

            // Grab the SMD
            $smd = $server->getServiceMap();

            // Return the SMD to the client
            header('Content-Type: application/json');
            echo $smd;
            return;
        }

        $server->handle();

    }
    function indexAction()
    {


    }

}
class API
{

    public function __construct()
    {
        $this->siteDAO = new Application_Model_SiteDAL();
        $this->menuDAO = new Application_Model_SiteMenuDAL();
    }

    //----------------------- Sites -----------------------------
    function siteList()
    {
        return $this->siteDAO->get_sites();

    }

    //Valid syntax when passing:
    function addSite($input,$token)
    {
        $site = $this->siteDAO->add_site($input);
        if(!$site)
        {
           return array("id" => true);
        }
        else
        {

            return array("id" => $site);
        }

    }

    function updateSite($id,$data)
    {
        /* returns false in case of no data change and error */
        if($this->siteDAO->update_site($id,$data))
            return true;

        return false;

    }

    function deleteSite($id)
    {
        return $this->siteDAO->delete_site($id);
    }

   //------------------------ Pages ----------------------------

    function getPagesList($id)
    {
        return $this->menuDAO->get_site_pages_list($id);
    }

    function addPage($data)
    {
        return $this->menuDAO->insert_page($data); //returns new page's id
    }
    function updatePage($id,$data)
    {
        return $this->menuDAO->update_page($id,$data);
    }
    function deletePage($id)
    {
        return $this->siteDAO->delete_site($id);
    }

}