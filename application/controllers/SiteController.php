<?php

class SiteController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function viewAction()
    {

        $title = $this->_getParam("site");
        $page =  $this->_getParam("page");
        $path =  "$title/$page";


        $DAO = new Application_Model_SiteDAL();
        $menu = new Application_Model_SiteMenuDAL();

        $site = $DAO->get_site_page("site_name",$title,$page);
        if(!$site)
        {
            throw new Zend_Controller_Action_Exception('This page does not exist', 404);
        }
        else //The page is ok, load menu.
        {
            //We would like to show the menu on each page
            $menuDAO  = new Application_Model_SiteMenuDAL();
            $menu  = $menuDAO->get_menu($site['site_id']);
            if(!$menu)
            {
                throw new Zend_Controller_Action_Exception('Error loading menu...', 404);
            }
            else
            {
                //$this->_helper->layout->setLayout('layoutName');
                $layout = Zend_Layout::getMvcInstance();
                $view = $layout->getView();
                $view->menu = $menu;
                $view->page = $site;
            }

        }
    }


}

