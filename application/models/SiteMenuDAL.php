<?php

class Application_Model_SiteMenuDAL
{

    function __construct()
    {
        $this->db = new Application_Model_DbTable_SiteMenu();
    }
    function get_menu($id)
    {
        try{

            $sql = $this->db->select()
                ->from("site_menu",array('title','friendly_url'))
                ->where("site_menu.site_id = '$id'");

            $result = $this->db->fetchAll($sql);
            if($result != "")
                return $result->toArray();
            else
                return false;
        }
        catch(Zend_Db_Exception $e)
        {
            echo $e;
            return false;
        }
    }
    function get_site_pages_list($id)
    {
        try{

            $sql = $this->db->select()
                ->from("site_menu",array('id','title','friendly_url','language'))
                ->where("site_menu.site_id = '$id'");

            $result = $this->db->fetchAll($sql);
            if($result != "")
                return $result->toArray();
            else
                return false;
        }
        catch(Zend_Db_Exception $e)
        {
            echo $e;
            return false;
        }
    }

    function insert_page($data)
    {
          try{
                $this->db->insert($data);
                return $this->db->getAdapter()->lastInsertId();
            }
            catch(Zend_Db_Exception $e)
            {
                echo $e;
                return false;
            }
    }

    function update_page($id,$data)
    {
        try{

            $count = $this->db->update($data,"id=$id");
            return ($count > 0) ? true : false;

        }
        catch(Zend_Db_Exception $e)
        {
            echo $e;
            return false;
        }
    }

    function delete_page($id)
    {
        try{
            if($this->db->delete("id=$id") == 1)
                return true;
            return false;
        }
        catch(Zend_Db_Exception $e)
        {
            echo $e;
            return false;
        }
    }

}

