<?php

class Application_Model_SiteDAL
{
    public function __construct()
    {
        $this->db = new Application_Model_DbTable_Site();
    }

    /*
       The key and value uses us for getting a site from the
       table with any "where" we want to use
        ~Get homepage by default~
    */

    function get_site($key,$value)
    {
        try{

            $sql = $this->db->select()
                ->from("site")
                ->where("site.$key = '$value'");

            $result = $this->db->fetchRow($sql);
            if($result != "")
                return $result->toArray();
            else
                return false;
        }
        catch(Zend_Db_Exception $e)
        {
            echo $e;
            return false;
        }
    }

    function get_site_page($key,$value,$page = "home")
    {
        try{

            $sql = $this->db->select()
                ->from("site")
                ->where("site.$key = '$value'")
                ->where("page.friendly_url = '$page'")
                ->join(array("page" => "site_menu"),"page.site_id = site.id");

            $sql->setIntegrityCheck(false); //todo: haven't found another way to join tables correctly with DAL/DAO(!?)

            $result = $this->db->fetchRow($sql);
            if($result != "")

                return $result->toArray();
            else
                return false;
        }
        catch(Zend_Db_Exception $e)
        {
            echo $e;
            return false;
        }
    }
    function get_sites($where = null,$limit = null)
    {
        try{
            $sql = $this->db->select();
            if(isset($limit) & is_numeric($limit) & $limit > 0)
                $sql->limit($limit);

            return $this->db->fetchAll($sql)->toArray();
        }
        catch(Zend_Db_Exception $e)
        {
            echo $e;
            return false;
        }

    }
    function add_site($data)
    {
        try{
            $this->db->insert($data);
            return $this->db->getAdapter()->lastInsertId();
        }
        catch(Zend_Db_Exception $e)
        {
            echo $e;
            return false;
        }

    }
    function update_site($id,$data)
    {
        try{

            $count = $this->db->update($data,"id=$id");
            return ($count > 0) ? true : false;

        }
        catch(Zend_Db_Exception $e)
        {
            echo $e;
            return false;
        }
    }
    function delete_site($id)
    {
        try{
            if($this->db->delete("id=$id") == 1)
                return true;
            return false;
        }
        catch(Zend_Db_Exception $e)
        {
            echo $e;
            return false;
        }
    }


}

