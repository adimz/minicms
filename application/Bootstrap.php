<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{

    protected function _initDb(){
        $config = new Zend_Config_Ini(APPLICATION_PATH.'/configs/application.ini','production');
        $db = Zend_Db::factory($config->database);

        Zend_Db_Table::setDefaultAdapter($db);

        Zend_Registry::set("db", $db);

}
    protected function _initRoutes()
    {
        $front =  Zend_Controller_Front::getInstance();
        $router =$front->getRouter();
        include APPLICATION_PATH . "/configs/routes.php";
    }

    protected function _initUrl()
    {
        $config = new Zend_Config_Ini(APPLICATION_PATH.'/configs/application.ini','production');
        $baseUrl = $config->base_url;
        define('BASE_URL', $baseUrl);

    }

    protected function _initConstants()
    {
        define('NO_CHANGE',-1);
    }

}
